# Review: Object-Oriented Design Principles

In this course, you will improve your Object-Oriented software design skills.
All of the new methods you will learn in this course build on the basic OO
design principles you learned in CS-140 and CS-242. You need to have a good
understanding of those principles to build on, so we will spend some time
reviewing them with your teammates and classmates. 

## Content Learning Objectives

After completing this activity, students should be able to:

- Define each of the principles.
- Explain why each of the principles is important.
- Give an example of the use of each of the principles.

## Process Skill Goals

During the activity, students should make progress toward:

- Leveraging prior knowledge and experience of other students. (Teamwork)
- Refining writing based on feedback. (Oral and Written Communication)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: Object-Oriented Design Principles

- Abstraction
- Encapsulation
- Polymorphism
- Inheritance

### Questions

1. Definitions (8 min)

    As a team, write a definition for each of the following terms.
    **Do not look up definitions - write your own as a team.**

    - Abstraction
    - Encapsulation
    - Polymorphism
    - Inheritance

    ***STOP - Do not continue until asked to do so by the instructor.***

2. Revising definitions (8 min)

    Based on other teams’ definitions, revise your team’s definition for each
    of the following terms:

    - Abstraction
    - Encapsulation
    - Polymorphism
    - Inheritance

        ***STOP - Do not continue until asked to do so by the instructor.***

3. Why important/useful, example (12 min)

    For each of the following terms, explain why it is important/useful in
    programming. Give an example.

    - Abstraction
    - Encapsulation
    - Polymorphism
    - Inheritance

    ***STOP - Do not continue until asked to do so by the instructor.***

4. Concrete vs. Abstract vs. Interface (9 min)

    - Explain the difference between a (Concrete) Class and an Abstract Class.
    - Explain the difference between a (Concrete) Class and an Interface.
    - Explain the difference between an Abstract Class and an Interface.

Copyright © 2021 Karl R. Wurst.

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
